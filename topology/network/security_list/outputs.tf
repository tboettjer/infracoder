output "list_id" {
  description = "Oracle Cloud Identifier for the main security list. "
  value       = oci_core_security_list.main_security_list.id
}