module "main_security_list" {
  source                 = "git::https://gitlab.com/tboettjer/infracoder.git//topology/network/security_list"
  compartment_ocid       = var.compartment_ocid
  vcn_id                 = oci_core_vcn.main_vcn.id
  sl_display_name        = "main_security_list"
  egress_destination     = "0.0.0.0/0"
  egress_protocol        = "all"
  ingress_stateless      = "false"
  ingress_security_rules = [
    {
      protocol_nr = "6"  // tcp
      port_min    = 22
      port_max    = 22
      source_cidr = "0.0.0.0/0"
    },
  ]
}