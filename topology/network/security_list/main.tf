resource "oci_core_security_list" "main_security_list" {
  compartment_id = var.compartment_ocid
  vcn_id         = var.vcn_id
  display_name   = var.sl_display_name
 
  egress_security_rules {
    destination  = var.egress_destination
    protocol     = var.egress_protocol
  }

  dynamic "ingress_security_rules" {
    for_each = [for rule in var.ingress_rules: {
      protocol_nr = rule.protocol_nr
      port_min    = rule.port_min
      port_max    = rule.port_max
      source_cidr = rule.source_cidr
    }]

    content {
      protocol  = ingress_security_rules.value.protocol_nr
      source    = ingress_security_rules.value.source_cidr != "" ? ingress_security_rules.value.source_cidr : "0.0.0.0/0"
      stateless = var.ingress_stateless
      tcp_options  {
        min = ingress_security_rules.value.port_min
        max = ingress_security_rules.value.port_max
      }
    }
  }
}