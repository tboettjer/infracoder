#############################################################
###### variables.tf #########################################
######                                                  #####
###### OCI Exadata Cloud Service Network Implementation #####
###### Based on Oracle documentation as of 2019-09-18   #####
######                                                  #####
###### contact: alexander.boettcher@oracle.com          #####
#############################################################
# Output the private and public IPs of the instances after terraform completes
#output "AllServicesOutput" {
#   value = ["${lookup(data.oci_core_services.service_gateway_all_oci_services.services[0], "cidr_block")}"]
#}
