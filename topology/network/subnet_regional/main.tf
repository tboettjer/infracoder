resource "oci_core_subnet" "regional_subnets" {
  for_each          = var.subnets
  compartment_id    = var.compartment_ocid
  vcn_id            = var.vcn_id
  security_list_ids = [ data.terraform_remote_state.main_vcn.outputs.main_security_list_id ]
  dns_label         = each.key
  display_name      = "${each.key}_subnet"
  cidr_block        = each.value
}