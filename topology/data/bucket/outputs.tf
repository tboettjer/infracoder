output "bucket_id" {
  value = oci_objectstorage_bucket.service_bucket.bucket_id
}

output "buckets" {
  value = data.oci_objectstorage_bucket_summaries.buckets.bucket_summaries
}

output "namespace" {
  value = data.oci_objectstorage_namespace.ns.namespace
}

output "metadata_namespace" {
  value  = data.oci_objectstorage_namespace_metadata.namespace_metadata.namespace
}

output "S3_compartment" {
  value  = data.oci_objectstorage_namespace_metadata.namespace_metadata.default_s3compartment_id
}
