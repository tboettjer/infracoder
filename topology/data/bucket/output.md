
## --- object storage ---
output "buckets" {
    value       = module.bucket.buckets
    description = "Buckets"
}

output "bucket_id" {
    value       = module.bucket.bucket_id
    description = "TFState Bucket"
}

output "namespace" {
    value       = module.bucket.namespace
    description = "Namespace"
}

