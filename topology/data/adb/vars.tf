# --- input variables ---
variable "compartment_ocid" {}
variable "type" {}
variable "db_name" {}
variable "display_name" {}

# --- settings  ---
variable "cpu_core_count" { default = "1" }
variable "data_storage_tbs" { default = "1" }
variable "is_free_tier" { default = "false" }               # true or false
variable "autoscaling_enabled" { default = "false" }        # true or false
variable "license_model" { default = "LICENSE_INCLUDED" }   # LICENSE_INCLUDED or BRING_YOUR_OWN_LICENSE
variable "whitelisted_ips" { default = [ "0.0.0.0/0" ] }
