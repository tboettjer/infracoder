## --- ... ---
data "oci_database_autonomous_databases" "autonomous_data_warehouses" {
  #Required
  compartment_id = var.compartment_ocid

  #Optional
  display_name = oci_database_autonomous_database.autonomous_data_warehouse.display_name
  db_workload  = var.autonomous_data_warehouse_db_workload
}

## --- ... ---
data "oci_database_autonomous_database_wallet" "autonomous_data_warehouse_wallet" {
  autonomous_database_id = oci_database_autonomous_database.autonomous_data_warehouse.id
  password               = random_string.autonomous_data_warehouse_wallet_password.result
  base64_encode_content  = "true"
}

