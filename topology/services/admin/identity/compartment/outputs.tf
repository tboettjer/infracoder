// Copyright (c) 2017, 2019, Oracle and/or its affiliates. All rights reserved.

######################
# compartment output #
######################

output "compartments" {
  value = "${data.oci_identity_compartments.compartments1.compartments}"
}

output "compartment_id" {
  value = "${oci_identity_compartment.default-compartment.id}"
}