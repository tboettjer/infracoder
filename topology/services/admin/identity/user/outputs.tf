// Copyright (c) 2017, 2019, Oracle and/or its affiliates. All rights reserved.

###############
# user output #
###############

output "default-user" {
  value = "${data.oci_identity_users.default-user.users}"
}

output "user_id" {
  value = "${oci_identity_user.default-user.id}"
}

output "default-pwd" {
  sensitive = false
  value     = "${oci_identity_ui_password.default-pwd.password}"
}

output "user-api-key" {
  value = "${oci_identity_api_key.api-key1.key_value}"
}

output "auth-token" {
  value = "${oci_identity_auth_token.auth-token1.token}"
}

output "customer-secret-key" {
  value = [
    "${oci_identity_customer_secret_key.customer-secret-key1.key}",
    "${data.oci_identity_customer_secret_keys.customer-secret-keys1.customer_secret_keys}",
  ]
}