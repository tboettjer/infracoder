## --- Oracle Kubernetes Engine ---

```
module "oke" {
  source                      = "git::https://gitlab.com/tboettjer/infracoder.git//topology/services/core/container"
  tenancy_ocid                = var.tenancy_ocid
  compartment_ocid            = var.compartment_ocid
  ssh_public_key              = var.node_pool_ssh_public_key
  instance_shape              = var.instance_shape
  image_ocid                  = var.image_ocid
  availability_domain         = var.availability_domain
  network_id                  = module.network.vcn_id
  nodePool_Subnet_1           = module.network.subnet_ids[0]
  nodePool_Subnet_2           = module.network.subnet_ids[1]
  nodePool_Subnet_3           = module.network.subnet_ids[2]
  cluster_kubernetes_version  = "v1.15.7"
  cluster_name                = "OKE"
}
```