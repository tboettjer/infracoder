# Manage Deployment Plans on GitLab

Creating definition files is teamwork, the infrastructure code base should be managed and distributed using a central repository. Git has become the defacto standard for version control and code sharing. Initially we recommend using a GitLab account, in later stage the project can move to a private instance. 

## Command Syntax

Installing Git on a server extends the shell command set. The syntax is similar to unix user commands. [Pro Git](https://git-scm.com/book/en/v2) provides a comprehensive documentation of all git commands.

```
git [--option] <command> [<arguments>]
```

Sharing code only requires a small subset of the git functionality. The [Github Cheat Sheet](https://github.github.com/training-kit/downloads/github-git-cheat-sheet.pdf) provides a comprehensive selection of the commands, typically used.


## Setup a Git Client

In addition to that, we can familerize ourself with the basic syntax using 'help'. With **git help _verb_** helps to retrieve the documentation for a specific command. To use git, a user identity is required. This information is associated with code that we commit to a repository.

```
git config --global user.name "YourName"
git config --global user.email "your.name@oracle.com"
```

On most Unix systems *Vim* is configured as default editor, not necessarily the easiest program for beginners. Therefor we change the default setting and use *Nano* instead. 

```
git config --global core.editor nano
```

### Start a Local Repository

There are two ways to initialise a git repository. `git clone` retrieves an existing project from a git server and creates a local repository and `git init` starts with creating a local directory. These commands create a hidden '.git' sub-directory that contains snapshots of the code changes. The InfraCoder image contains a directory '/home/$username/infracoder/' that we want to use as the InfraCoder repository.

```
cd ~/infracoder/ && git init
```

After initializing the repository, we specify a .gitignore file to prevent the syncronization of files that contain secrets. The [.gitignore file](.gitignore) in the root directory of the infracoder directory might be a good start. For more complex projects, GitHub maintains a fairly comprehensive list of good [.gitignore file examples](https://github.com/github/gitignore) for dozens of projects. 

Commiting code in git is a two step process. A staging step allows continue making changes in the working directory before commiting new code to version control. This allows to record smaller adjustments before publishing code changes to a broader audience. Initially we use the 'add .' command to transfers all existing data within the current directory to the staging area. The dot **.** refers to 'all'. 

```
git add .
```

With 'status' we check which files have been transfered to the staging area. The repsonse should show all files in the current directory

```
git status
```  

Now we can use *-commit* to create a snapshot and transfers the changes into the repository. The *-a* collects all changes made in the current directory and the *-m 'Initial Setup'* adds option adds the obligatory message helping engineers to track versions of the codebase.

```
git commit -a -m 'Initial Commit'
```

### Basic git commands

Create a folder with the project name, and copy the content of the infracoder directory. It order to facilitate a new repository, make sure that the ‚‘.gitignore‘ is also copied, but the ‚‘.git‘ directory is not.

```
export PROJECT=„your_project_name“

touch $HOME/$PROJECT
cp -r $HOME/infracoder/* $HOME/$PROJECT/
cp $HOME/infracoder/.gitignore $HOME/$PROJECT/.gitignore
rm -r $HOME/$PROJECT/.git
```

Intialize the project folder as a git repository

```
cd ~/$PROJECT/ && git init
```

Git is a version control system, it allows you to manage the different versions of your codebase. The codebase itself can be split into branches. Per intial branch is called „master“. Commiting new code to the master branch is a two step process, first code is written to a staging area and from their commited to the master branch. We check the current status of our folder.

```
git status
```

**Adding files to the local repository**

The „status“ command option shows that all files are „untracked“, which means, the files are not in the staging area. We can add them by typing the following command.

```
git add .
```

The „.“ option stands for „all“ and instructs git to include all files and directories in the current folder. When executing a „git status“ again, it will show „Changes to be commited“, which means the files have been added to the staging area. From there, files can be commited into the repository.

```
git commit -m „Initial Commit“
```

The -m option refers to a message that summarizes, which files have been changed. Typing „git status“ again shows nothing to commit, all changes have been captured. 

```
git log -oneline
```

With git log we can see a unique identifier for the commit incl. the message that we provided. When we add a new file, ‚git status‘ shows the files are untracked and not staged.Now, modify a nginx_ol77.sh file.

```
...
```

Add a sub-folder named templates to your git-test folder, and then add a file named test.html to the templates folder. Then set the contents of this file to be the same as the index.html file above.
Then check the status and add all the files to the staging area.
Then do the second commit to your repository
Now, modify the nginx_ol77.sh file as follows:

```
...
```
Now add the modified nginx_ol77.sh file to the staging area and then do a third commit

**Checking out a file from an earlier commit**
To check out the index.html from the second commit, find the number of the second commit using the git log, and then type the following at the prompt:

```
git checkout <second commit's number> nginx_ol77.sh
```

Resetting the Git repository

To discard the effect of the previous operation and restore index.html to its state at the end of the third commit, type:

```
git reset HEAD nginx_ol77.sh
```

Then type the following at the prompt:

```
git checkout -- nginx_ol77.sh
```

You can also use git reset to reset the staging area to the last commit without disturbing the working directory.

## Central Repositories

Shared configuration files, packer templates and terraform modules saves a lot of coding time. The reusability nature and reduces duplication, enables isolation, and enhances testability. A central git repository is the place for a team to pull code, contribute and reuse it. It enables the independent development of project components and prevents developers from blocking each other, because it helps to build independent project components. We start a repository by creating a local git directory on an InfraCoder instance and clone it on a GitLab server.

### Directory Structure

A formalized directory structure helps to organize infrastructure code in the repository. Adopting the outline as standard keeps the code clean and reusable. The InfraCoder image contains the "infracoder" directory as a blueprint for a repository structure.

```
.
├── README.md
├── docs
├── artifacts
│   ├── bash
│   └── packer
└── topology
    ├── main.tf
    ├── provider.tf
    ├── terraform.tf
    ├── network
    │   ├── datasources.tf
    │   ├── main.tf
    │   ├── outputs.tf
    │   └── vars.tf
    ├── databases
    │   ├── adw
    │   ├── dbcs
    │   ├── mysql
    │   └── exacs
    └── services
```

### Choose a Service Model

There are multiple options to create and share a git repository: Public repositories are shared via [GitHub](https://www.github.com), multi-company teams can maintain private repositories on [GitLab](https://www.gitlab.com) and closed user groups setup a private [Git server](https://cloudmarketplace.oracle.com/marketplace/en_US/listing/37990057). InfraCoders at Oracle can request access to the *[InfraCoder repository on GitLab](https://gitlab.com/infracoding)* to receive a URL for the project.



### Replicate the Code Base

Now we are ready to connect our local repository to GitLab and push the initial content

```
git remote add origin git@gitlab.com:infracoding/<project>.git && git push -u origin --all
```

The GitLab home directory should now look like this:

[<img src="docs/gitlab.png">](https://gitlab.com/infracoding)

Enable upstream replication with the InfraCoder repo

```
git remote add upstream https://gitlab.com/tboettjer/infracoder.git
```

### Handling Secrets
We collect this information and store in a text file [SecretsTenant.txt](artifacts/bash/template_SecretsTenant.txt). Following is an example of my text file. The `secrets*` entry in th .gitignore file prevents this information from being synchronized with the git server. 

## Resolving Differences

The main purpose of using a central repository is collaboration in a team. Hence, code changes are made locally and merged into a joined repository. It is good practice to start every coding session with a replication of the current content.

```
git pull
```

When two programmers work in paralell, consistency errors might occur the following commands help to automatically resolve such issues.

* **git log** - for finding specific commits by content of the messages or the difference 
* **git diff** - shows differences between development trees, e.g. between the working environment and the staging area, between staging area and last commit, or between two commits.

Another important feature of git is the management of different branches. However, InfraCoder teams are usually pretty small and code changes less frequent than in software projects, hence branches are sledmly needed.


[<<](packer.md) | [+](setup.md) | [>>](modules.md)