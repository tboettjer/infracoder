# Remote Management using the Command Line Interface (CLI)

The oci command supports regular expression to execute complex requests. Ressources are identified in a hierarcicle structure. E.g. 'oci' refers to all resources in a tenant, 'oci iam' to the identity and access management resources and 'oci iam compartments' to the compartments inside a tenant. Oracle has published a [Hands-on-Lab](https://oracle.github.io/learning-library/oci-library/DevOps/OCI_CLI/OCI_CLI_HOL.html) to get familar with the oci command syntax syntax.

```
oci [--option] <command> [<arguments>]
```

We are using the same methodology and create modules using functions that was introduced for server configurations. Only, remote management modules are stored in an own library, the liboci.sh. 

```
function () {
  oci [--option] <command> [<arguments>]
}
```

We rely on the same helper- and admin functions that we used to configure a server. Other than the initial webshell install the bootstrap script is initiated as $USERNAME without 'sudo', even though some functions requirre root rights, hence the password will be required. 

## The Init Script

The [init.sh](artifacts/bash/init.sh) script automates the provisioning of a minimum set of network ressources.

* It creates a virtual cloud network (VCN)
* defines a set of rules for security lists
* creates a subnet and
* launches an internet gateway

### File Structure

An bootstrap script uses the same outline introduced for server configurations. The first section sources modules with admin and remote functions, the second uses the main() function to provision the ressources and the third section contains the trigger to execute the provisioing procedure. 

```
#!/bin/bash

<service header>
<settings and variables>

# --- sources ---
source libadmin
source liboci

# --- modules ---
main () {
  function_1
  function_2
  ...
}

# --- execution ---

init
main

exit 0
```

### Selecting Compartments

Compartments represents the root of an admin tree in OCI. Any resource in OCI is associated with a [compartment](https://docs.cloud.oracle.com/iaas/Content/Identity/Tasks/managingcompartments.htm). Initially a single compartment is sufficient, however over time the compartment structure will be extended to match the organizational structure of an IT department. Network- and database administration is separated, contractors, outsourcing partners and/ or managed service providers receive an own compartments. 

Note: The initial compartments is defined on [Level 1](https://docs.cloud.oracle.com/en-us/iaas/Content/Identity/Tasks/managingcompartments.htm), some scripts will not run with subcompartments.

**Working with CLI Outputs**

The CLI provides a reponse in JSON, which incl. all meta-data per compartment. For larger tenants this output becomes hard to read. There are two ways to extract information from JSON files. 

**JMESPath** - The oci library contains a runtime for [JMESPath](http://jmespath.org/) to simplify JSON messages. This functionality is invoked with the --query option. Main features are filters and multiselects to allow engineers extracting single or multiple keys in the array of JSON objects. E.g. taking a list of server instances and, read out the OCIDs.

```
oci iam compartment list --output table --all --query "data [*].{Name:\"name\", OCID:id}"
```

**jq** - Automation engineers retrieve JSON messages from various sources, not just from the CLI. Using an own command instead of invoking CLI functionality provides automation engineers with more flexibility. [jq](https://stedolan.github.io/jq/) is a generic parser that can simplify oci responses by using a pipe. Tutorials like [reshaping JSON with jq](https://programminghistorian.org/en/lessons/json-and-jq) or [parse JSON data using jq](https://medium.com/how-tos-for-coders/https-medium-com-how-tos-for-coders-parse-json-data-using-jq-and-curl-from-command-line-5aa8a05cd79b) provide an introduction for the jq syntax. The [jq playground](https://jqplay.org/) helps to design complex filters.

```
oci iam compartment get -c $COMPARTMENT | jq --raw-output .data.id
```

### Settings and Variables

The variables section for the bootstrap process reflects the network configuration requirements. The script allows for the use project names to separate multiple management environments. By default the `project=""` option can be left empty. The management network has to be associated with a management compartment. The OCID is provided with the cmprt variable. By default we allow egress traffic to any server and restrict ingress traffic to a minimum. In our example we only open the ports for SSH, HTTP and HTTPS. Before running the script 

```
# --- settings and variables ---
read -e -p "Please provide a compartment OCID: " COMPARTMENT
# use names to separate environments. If you don't want to use it, leave project=""
read -e -p "Name of the project: " -i "InfraCoder" PROJECT

if [ "$PROJECT" != "" ]; then 
    PROJECT_=$(echo $PROJECT\_ | tr [A-Z] [a-z] | sed "s/ /\_/g")
else
    PROJECT_="default_";
fi

declare -a INGRESS_PORTS
INGRESS_PORTS+=(22 80 443)

TENANCY=$(grep tenancy ~/.oci/config | cut -f 2 -d '=')
USER_OCID=$(grep user ~/.oci/config | cut -f 2 -d '=')
USER_FINGERPRINT=$(grep fingerprint ~/.oci/config | cut -f 2 -d '=')
LOCATION=$(grep region ~/.oci/config | cut -f 2 -d '=')
```

## Create Functions

### Network Modules

The Virtual Cloud Network (VCN) is a layer three domain that includes own network functions. 

```
create_vcn () {
    # the DNS label for the VCN must not contain non-alphanumeric charactes and only 15 characters are alowed at most
    echo_log "creating a VCN"

    DNSLABEL=$(echo "${PROJECT_}vcn" | tr [A-Z] [a-z] | sed "s/\_//g")

    VCN=$(oci network vcn create \
    --wait-for-state AVAILABLE \
    --cidr-block 10.0.0.0/24 \
    --compartment-id $COMPARTMENT \
    --region $REGION \
    --dns-label $DNSLABEL \
    --display-name "${PROJECT_}VCN" | jq --raw-output .data.id);

    echo_log "VCN created, name: ${PROJECT_}VCN, region: ${LOCATION}"
}
```

Within the VCN we reserve an address space for the deployment of server that can be accessed over the internet. Initially we only work with one IP address space, this can be extended later. However, the overall address space is limited by the size of the CIDR block defined in the VCN. Changing the size requires a rebuild of the network.

```
create_subnet () {
    echo_log "creating subnet"

    SECURITYLIST=$(oci network security-list create \
    --region $REGION \
    --display-name "${PROJECT_}SL" \
    --vcn-id $VCN -c $COMPARTMENT \
    --egress-security-rules "[ $EGRESS_SECURITY ]" \
    --ingress-security-rules "$INGRESS_SECURITY" | jq --compact-output [.data.id]);

    SUBNET=$(oci network subnet create \
    --region $REGION \
    --cidr-block 10.0.0.0/25 \
    -c $COMPARTMENT \
    --display-name "${PROJECT_}SBNT" \
    --vcn-id $VCN \
    --security-list-ids $SECURITYLIST | jq --raw-output .data.id)

    echo_log "Subnet created and security list associated, name: ${PROJECT_}SBNT"
}
```

On the subnet level we define security lists that limit the ports that are accessible over the internet.

```
create_rules () {
    # Egress rules
    EGRESS_SECURITY='{"destination": "0.0.0.0/0", "destination-type": "CIDR_BLOCK", "protocol": "all", "isStateless": false}'
    
    # Ingress rules. The script opens these ports both on network level (security list) as well as on O/S level (firewall-cmd)
    declare -a INGRESS_RULES
    
    for PORT in ${INGRESS_PORTS[@]}; do
        define_rule () {
            cat <<EOF
            {"source": "0.0.0.0/0", "source-type": "CIDR_BLOCK", "protocol": 6, "isStateless": false, "tcp-options": {"destination-port-range": {"max": $PORT, "min": $PORT}}}
EOF
        };
        INGRESS_RULES+=$(define_rule)    
    done;

    INGRESS_SECURITY=$(echo ${INGRESS_RULES[*]} | jq . -s)
}
```

After creating a network space we attach an internet gateway. Exposing servers to the internet requires a public IP address. While the VCN creates an internal address space the internetgateway is required to assign a public IP to a server. 

```
enable_communication () {
    echo_log "creating an Internet Gateway and adding default route"

    INTERNET_GATEWAY=$(oci network internet-gateway create \
    --region $REGION \
    -c $COMPARTMENT \
    --is-enabled true \
    --vcn-id $VCN \
    --display-name "${PROJECT_}IGW" | jq --compact-output '. | [{cidrBlock:"0.0.0.0/0",networkEntityId: .data.id}]')

    ROUTE_TABLE=$(oci network route-table list \
    --region $REGION \
    -c $COMPARTMENT \
    --vcn-id $VCN | jq --raw-output '.data[].id')

    oci network route-table update \
    --region $REGION \
    --rt-id $ROUTE_TABLE \
    --route-rules $INTERNET_GATEWAY \
    --force
    
    echo_log "Internet gateway created, name: ${PROJECT_}IGW"
}
```
### Admin Modules

To prepare the bootstrapping process we execute a script the adds tenancy credentials to the .credentials and .credentials files. 

```
tenancy_configuration () {
    CREDENTIALS=$HOME/.credentials
    CREDENTIALS=$HOME/.credentials

    sudo bash -c "mv /tmp/id_rsa $HOME/id_rsa"
    sudo bash -c "mv /tmp/id_rsa.pub $HOME/id_rsa.pub"

    if [ ! $(grep -qw "tenancy settings" $CREDENTIALS  && echo $?) ]; then
        echo " " >> $CREDENTIALS
        echo "# --- tenancy settings ---" >> $CREDENTIALS
        echo "COMPARTMENT=${COMPARTMENT}" >> $CREDENTIALS
        echo "LOCATION=${LOCATION}" >> $CREDENTIALS
        echo "PROJECT=${PROJECT}" >> $CREDENTIALS
    fi
    
    if [ ! $(grep -qw "tenancy credentials" $CREDENTIALS  && echo $?) ]; then
        echo " " >> $CREDENTIALS
        echo "# --- tenancy credentials ---" >> $CREDENTIALS
        echo "TENANCY=${TENANCY}" >> $CREDENTIALS
        echo "USER_OCID=${USER_OCID}" >> $CREDENTIALS
        echo "USER_FINGERPRINT=${USER_FINGERPRINT}" >> $CREDENTIALS
    fi
}
```

## Executing the Script

Working with the CLI requires that the tenant configuration incl. a valid key pair is stored in `$HOME/.oci/config`. We can check whether the fingerprint in the config file matches a fingerprint of the respective user in the web console or execute a short 'get' command, which returns the name of the tenant in JSON format when the CLI is configured corrrectly.

```
oci os ns get
```

The complete provisioning procedure is specified in the main function.

```
main () {
    echo_log "kicking off"
    tenancy_configuration
    create_vcn
    create_rules
    create_subnet
    enable_communication
}
```

After confirming the remote connection we execute the [bootstrap script](artifacts/bash/bootstrap.sh). 

```
sudo chmod +x ~/infracoder/artifacts/bash/bootstrap.sh && ~/infracoder/artifacts/bash/bootstrap.sh
```

[<<](bash.md) | [+](setup.md) | [>>](packer.md)