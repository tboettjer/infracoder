variable "compartment_ocid" {
  type = string
  default = "VAR_COMPARTMENT"
  description = "The OCID of the compartment, used to generate custom images"
}

variable "subnet_ocid" {
  type = string
  default = "VAR_SUBNET" 
  description = "The OCID of the subnet, used to generate custom images"
}

variable "region" {
  type = string
  default = "VAR_REGION"
  description = "Deployment Region for custom images"
}

variable "availability_domain" {
  type = string
  default = "VAR_AD"
  description = "Availability domain for custom images"
}

variable "image_ocid" {
  type = string
  default = "VAR_BASEIMAGE"
  description = "The OCID of the base image for the creation of a custom image"
}

variable "image_name" {
  type = string
  default = "VAR_NAME"
  description = "Name of the custom image in the image store"
}

variable "shape" {
  type = string
  default = "VAR_SHAPE"
  description = "Shape used to build a custom image"
}